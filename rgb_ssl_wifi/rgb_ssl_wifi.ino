#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServerSecure.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>

const char* SSID = "DosCerveza";
const char* PASSWORD = "5a4253d6";

//ESP8266WebServer server(80);
BearSSL::ESP8266WebServerSecure server(443);

static const char serverCert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDSzCCAjMCCQD2ahcfZAwXxDANBgkqhkiG9w0BAQsFADCBiTELMAkGA1UEBhMC
VVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDU9yYW5nZSBDb3VudHkx
EDAOBgNVBAoMB1ByaXZhZG8xGjAYBgNVBAMMEXNlcnZlci56bGFiZWwuY29tMR8w
HQYJKoZIhvcNAQkBFhBlYXJsZUB6bGFiZWwuY29tMB4XDTE4MDMwNjA1NDg0NFoX
DTE5MDMwNjA1NDg0NFowRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3Rh
dGUxITAfBgNVBAoMGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDCCASIwDQYJKoZI
hvcNAQEBBQADggEPADCCAQoCggEBAPVKBwbZ+KDSl40YCDkP6y8Sv4iNGvEOZg8Y
X7sGvf/xZH7UiCBWPFIRpNmDSaZ3yjsmFqm6sLiYSGSdrBCFqdt9NTp2r7hga6Sj
oASSZY4B9pf+GblDy5m10KDx90BFKXdPMCLT+o76Nx9PpCvw13A848wHNG3bpBgI
t+w/vJCX3bkRn8yEYAU6GdMbYe7v446hX3kY5UmgeJFr9xz1kq6AzYrMt/UHhNzO
S+QckJaY0OGWvmTNspY3xCbbFtIDkCdBS8CZAw+itnofvnWWKQEXlt6otPh5njwy
+O1t/Q+Z7OMDYQaH02IQx3188/kW3FzOY32knER1uzjmRO+jhA8CAwEAATANBgkq
hkiG9w0BAQsFAAOCAQEAnDrROGRETB0woIcI1+acY1yRq4yAcH2/hdq2MoM+DCyM
E8CJaOznGR9ND0ImWpTZqomHOUkOBpvu7u315blQZcLbL1LfHJGRTCHVhvVrcyEb
fWTnRtAQdlirUm/obwXIitoz64VSbIVzcqqfg9C6ZREB9JbEX98/9Wp2gVY+31oC
JfUvYadSYxh3nblvA4OL+iEZiW8NE3hbW6WPXxvS7Euge0uWMPc4uEcnsE0ZVG3m
+TGimzSdeWDvGBRWZHXczC2zD4aoE5vrl+GD2i++c6yjL/otHfYyUpzUfbI2hMAA
5tAF1D5vAAwA8nfPysumlLsIjohJZo4lgnhB++AlOg==
-----END CERTIFICATE-----
)EOF";

static const char serverKey[] PROGMEM =  R"EOF(
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA9UoHBtn4oNKXjRgIOQ/rLxK/iI0a8Q5mDxhfuwa9//FkftSI
IFY8UhGk2YNJpnfKOyYWqbqwuJhIZJ2sEIWp2301OnavuGBrpKOgBJJljgH2l/4Z
uUPLmbXQoPH3QEUpd08wItP6jvo3H0+kK/DXcDzjzAc0bdukGAi37D+8kJfduRGf
zIRgBToZ0xth7u/jjqFfeRjlSaB4kWv3HPWSroDNisy39QeE3M5L5ByQlpjQ4Za+
ZM2yljfEJtsW0gOQJ0FLwJkDD6K2eh++dZYpAReW3qi0+HmePDL47W39D5ns4wNh
BofTYhDHfXzz+RbcXM5jfaScRHW7OOZE76OEDwIDAQABAoIBAQDKov5NFbNFQNR8
djcM1O7Is6dRaqiwLeH4ZH1pZ3d9QnFwKanPdQ5eCj9yhfhJMrr5xEyCqT0nMn7T
yEIGYDXjontfsf8WxWkH2TjvrfWBrHOIOx4LJEvFzyLsYxiMmtZXvy6YByD+Dw2M
q2GH/24rRdI2klkozIOyazluTXU8yOsSGxHr/aOa9/sZISgLmaGOOuKI/3Zqjdhr
eHeSqoQFt3xXa8jw01YubQUDw/4cv9rk2ytTdAoQUimiKtgtjsggpP1LTq4xcuqN
d4jWhTcnorWpbD2cVLxrEbnSR3VuBCJEZv5axg5ZPxLEnlcId8vMtvTRb5nzzszn
geYUWDPhAoGBAPyKVNqqwQl44oIeiuRM2FYenMt4voVaz3ExJX2JysrG0jtCPv+Y
84R6Cv3nfITz3EZDWp5sW3OwoGr77lF7Tv9tD6BptEmgBeuca3SHIdhG2MR+tLyx
/tkIAarxQcTGsZaSqra3gXOJCMz9h2P5dxpdU+0yeMmOEnAqgQ8qtNBfAoGBAPim
RAtnrd0WSlCgqVGYFCvDh1kD5QTNbZc+1PcBHbVV45EmJ2fLXnlDeplIZJdYxmzu
DMOxZBYgfeLY9exje00eZJNSj/csjJQqiRftrbvYY7m5njX1kM5K8x4HlynQTDkg
rtKO0YZJxxmjRTbFGMegh1SLlFLRIMtehNhOgipRAoGBAPnEEpJGCS9GGLfaX0HW
YqwiEK8Il12q57mqgsq7ag7NPwWOymHesxHV5mMh/Dw+NyBi4xAGWRh9mtrUmeqK
iyICik773Gxo0RIqnPgd4jJWN3N3YWeynzulOIkJnSNx5BforOCTc3uCD2s2YB5X
jx1LKoNQxLeLRN8cmpIWicf/AoGBANjRSsZTKwV9WWIDJoHyxav/vPb+8WYFp8lZ
zaRxQbGM6nn4NiZI7OF62N3uhWB/1c7IqTK/bVHqFTuJCrCNcsgld3gLZ2QWYaMV
kCPgaj1BjHw4AmB0+EcajfKilcqtSroJ6MfMJ6IclVOizkjbByeTsE4lxDmPCDSt
/9MKanBxAoGAY9xo741Pn9WUxDyRplww606ccdNf/ksHWNc/Y2B5SPwxxSnIq8nO
j01SmsCUYVFAgZVOTiiycakjYLzxlc6p8BxSVqy6LlJqn95N8OXoQ+bkwUux/ekg
gz5JWYhbD6c38khSzJb0pNXCo3EuYAVa36kDM96k1BtWuhRS10Q1VXk=
-----END RSA PRIVATE KEY-----
)EOF";

const int LED_RED = D3;
const int LED_GREEN = D8;
const int LED_BLUE = D5;
const int FADE = 5;

int currentRed = 0,
  currentGreen = 0, 
  currentBlue = 0;

//---------------------color----------------------------------
int fadeTo(int red, int green, int blue){
  int duration = 0;
  while(!hasColor(LED_RED, red) || !hasColor(LED_GREEN, green) || !hasColor(LED_BLUE, blue) ){
    fade(LED_RED, red);
    fade(LED_GREEN, green);
    fade(LED_BLUE, blue);
    delay(FADE);
    duration += FADE;
  }
  return duration;
}

int fade(int led, int color){
  int currentValue = getCurrentValue(led);
  if(!hasColor(led, color)){
      if(currentValue <= color){
        currentValue++;
      }else{
        currentValue--;
      }
      analogWrite(led, currentValue);
      setCurrentValue(led, currentValue);
  }
}

boolean hasColor(int led, int color){
  if(getCurrentValue(led) == color){
    return true;
  }else{
    return false;
  }
}


int getCurrentValue(int led){
   switch(led){
    case LED_RED:
      return currentRed;
    case LED_GREEN:
      return currentGreen;
    case LED_BLUE:
      return currentBlue;  
  }
}

int setCurrentValue(int led, int value){
  switch(led){
    case LED_RED:
      currentRed = value;
      break;
    case LED_GREEN:
      currentGreen = value;
      break;
    case LED_BLUE:
      currentBlue= value;
      break;  
  }
}

//-----------------------------http handler---------------------------
void color() {
    Serial.println("-----------------color()");
    if(checkBody()){
      if(hasArg("r"), hasArg("g"), hasArg("b")){
        // check parameter
        int red = getArg("r").toInt();
        int green = getArg("g").toInt();
        int blue = getArg("b").toInt();
        Serial.println("start: changing color: \"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue) + ",\"received\": { \"r\":" + String(red) + ", \"g\":" + String(green) + ", \"b\":" + String(blue) +"}");
        int duration = fadeTo(red, green, blue);
        Serial.println("finish: changed: \"duration\":" + String(duration) + ", \"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue) + ",\"received\": { \"r\":" + String(red) + ", \"g\":" + String(green) + ", \"b\":" + String(blue) +"}");
        String message = createResponse("\"duration\":" + String(duration) + ", \"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue) + ",\"received\": { \"r\":" + String(red) + ", \"g\":" + String(green) + ", \"b\":" + String(blue) +"}");
        respond(message);
      }
    }
}

void turnOff() {
  Serial.println("-----------------turnOff()");
  int duration = fadeTo(0,0,0);
  respond(createResponse("\"duration\": " + String(duration)));
  analogWrite(LED_RED, 0);
  analogWrite(LED_GREEN, 0);
  analogWrite(LED_BLUE, 0);
  currentRed = 0;
  currentGreen = 0;
  currentBlue = 0;
}

void getColor(){
  Serial.println("-----------------getColor()");
  String message = createResponse("\"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue));
  respond(message);
}

//-----------------------------helper--------------------------------
String createResponse(String data){
  if(data.length() == 0){
      return "{ \"success\": true}";
  }else{
    return "{ \"success\": true," + data + "}";
  }

}

String createError(String data){
  return "{ \"success\": false, " + data + "}";
}

void respond(String message){
  Serial.println(message);
  //server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  //server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  //server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "text/plain", message);
}

void respondWithError(String error){
    Serial.println(error);
    server.send(500,"text/plain", error);
}

boolean checkBody(){
  /*if (server.hasArg("plain")== false){ //Check if body received
    Serial.println( "{ \"success\": false, \"message\": \"body is empty\"}");
    server.send(500, "text/plain", "{ \"success\": false, \"message\": \"body is empty\"}");
    return false;
  }*/
  return true;
}

boolean hasArg(String key){
 if (server.hasArg(key) == false){
    Serial.println( "{ \"success\": false, \"message\": \"" + key + " is empty\"}");
    server.send(500, "text/plain", "{ \"success\": false, \"message\": \"" + key + " is empty\"}");
    return false;
  }
  return true;
}

String getArg(String key){
  return server.arg(key);
}

void handleNotFound(){
  Serial.println("-----------------handleNotFound()");
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  Serial.begin(9600);
  WiFi.mode(WIFI_AP_STA);
  WiFi.begin(SSID, PASSWORD);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("try to connect to Wlan.");
  }
  
  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");
  MDNS.begin("test");
  server.setRSACert(new BearSSLX509List(serverCert), new BearSSLPrivateKey(serverKey));
  
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(SSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  server.on("/off", turnOff);

  server.on("/color", color);

  server.on("/", HTTP_OPTIONS, []() {
    //server.sendHeader("Access-Control-Max-Age", "10000");
    //server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
    //server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    //server.send(200, "text/plain", "" );
  });

  server.on("/getColor", getColor);
  
  server.onNotFound(handleNotFound);
  
  server.begin();

  MDNS.addService("https", 443);
  Serial.println("HTTPs server started");

  pinMode(LED_RED, OUTPUT); 
  pinMode(LED_GREEN, OUTPUT); 
  pinMode(LED_BLUE, OUTPUT); 

}

void loop(void)
{
  //needed
  server.handleClient();
}
