import { Component, OnInit } from '@angular/core';
import { RestService } from './service/rest-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'rgb-app';

  constructor(private rest:RestService){}

  red = 0;
  green = 0;
  blue = 0;

  ColorChange(color: string, event: any) {
    switch (color) {
      case "r":
        this.red = event.value;
        break;
      case "g":
        this.green = event.value;
        break;
      case "b":
        this.blue = event.value;
        break;
    }
    console.log(event.value);
  }

  submit(){
    this.rest.setColor(this.red, this.green, this.blue)
      .then(function(object:object){
        console.dir(object);
      }).catch(function(object:object){
        console.dir(object);
      });
  }

  ngOnInit(){
    var that = this; 
    this.rest.getColor()
    .then(function(object:object){
      console.log(object);
      console.log(object["r"]);
      that.red = object["r"];
      that.green = object["g"];
      that.blue = object["b"];
    }).catch(function(object:object){
      console.dir(object);
    });
  }

  off(){
    var that = this; 
    this.rest.off()
    .then(function(object:object){
      console.dir(object);
      that.red = 0;
      that.green = 0;
      that.blue = 0;
    }).catch(function(object:object){
      console.dir(object);
    });
  }

  getColorAsString(){
    console.log("rgb(" + this.red + "," + this.green + "," + this.blue + ")");
    return "rgb(" + this.red + "," + this.green + "," + this.blue + ")";
  }
}
