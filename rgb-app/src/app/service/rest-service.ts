import { Injectable } from "@angular/core";
import { Http, Headers } from '@angular/http';

@Injectable()
export class RestService {

    constructor(private http: Http) { }

    server: string = "http://192.168.178.65:80";

    setColor(r: number, g: number, b: number): Promise<object> {
        var that = this;
        return new Promise(function (resolve, reject) {
            that.http.post(that.server + "/color?r=" + r + "&g=" + g + "&b=" + b, {
                "test":"test"
            }, {
                    headers: that.createHeader()
                }).subscribe(p => {
                    console.log(p["_body"]);
                    p = JSON.parse(p["_body"]);
                    resolve(p);
                }, error => {
                    console.log(error);
                    resolve(error);
                });
        });
    }

    getColor(): Promise<object> {
        var that = this;
        return new Promise(function (resolve, reject) {
            that.http.get(that.server + "/getColor", {
                headers: that.createHeader()
            })
                .subscribe(p => {
                    console.log(p["_body"]);
                    p = JSON.parse(p["_body"]);
                    resolve(p);
                }, error => {
                    reject(error);
                });
        });
    }

    off(): Promise<object> {
        var that = this;
        return new Promise(function (resolve, reject) {
            that.http.get(that.server + "/off", {
                headers: that.createHeader()
            })
                .subscribe(p => {
                    console.log(p["_body"]);
                    p = JSON.parse(p["_body"]);
                    resolve(p);
                }, error => {
                    reject(error);
                });
        });
    }

    createHeader():Headers{
        var header = new Headers();
        header.append("Access-Control-Allow-Origin", "*");
        return 
    }
}