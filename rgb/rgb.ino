
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* SSID = "DosCerveza";
const char* PASSWORD = "5a4253d6";

ESP8266WebServer server(80);

const int LED_RED = D3;
const int LED_GREEN = D8;
const int LED_BLUE = D5;
const int FADE = 5;

int currentRed = 0,
  currentGreen = 0, 
  currentBlue = 0;

//---------------------color----------------------------------
int fadeTo(int red, int green, int blue){
  int duration = 0;
  while(!hasColor(LED_RED, red) || !hasColor(LED_GREEN, green) || !hasColor(LED_BLUE, blue) ){
    fade(LED_RED, red);
    fade(LED_GREEN, green);
    fade(LED_BLUE, blue);
    delay(FADE);
    duration += FADE;
  }
  return duration;
}

int fade(int led, int color){
  int currentValue = getCurrentValue(led);
  if(!hasColor(led, color)){
      if(currentValue <= color){
        currentValue++;
      }else{
        currentValue--;
      }
      analogWrite(led, currentValue);
      setCurrentValue(led, currentValue);
  }
}

boolean hasColor(int led, int color){
  if(getCurrentValue(led) == color){
    return true;
  }else{
    return false;
  }
}


int getCurrentValue(int led){
   switch(led){
    case LED_RED:
      return currentRed;
    case LED_GREEN:
      return currentGreen;
    case LED_BLUE:
      return currentBlue;  
  }
}

int setCurrentValue(int led, int value){
  switch(led){
    case LED_RED:
      currentRed = value;
      break;
    case LED_GREEN:
      currentGreen = value;
      break;
    case LED_BLUE:
      currentBlue= value;
      break;  
  }
}

//-----------------------------http handler---------------------------
void color() {
    Serial.println("-----------------color()");
    if(checkBody()){
      if(hasArg("r"), hasArg("g"), hasArg("b")){
        // check parameter
        int red = getArg("r").toInt();
        int green = getArg("g").toInt();
        int blue = getArg("b").toInt();
        Serial.println("start: changing color: \"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue) + ",\"received\": { \"r\":" + String(red) + ", \"g\":" + String(green) + ", \"b\":" + String(blue) +"}");
        int duration = fadeTo(red, green, blue);
        Serial.println("finish: changed: \"duration\":" + String(duration) + ", \"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue) + ",\"received\": { \"r\":" + String(red) + ", \"g\":" + String(green) + ", \"b\":" + String(blue) +"}");
        String message = createResponse("\"duration\":" + String(duration) + ", \"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue) + ",\"received\": { \"r\":" + String(red) + ", \"g\":" + String(green) + ", \"b\":" + String(blue) +"}");
        respond(message);
      }
    }
}

void turnOff() {
  Serial.println("-----------------turnOff()");
  int duration = fadeTo(0,0,0);
  respond(createResponse("\"duration\": " + String(duration)));
  analogWrite(LED_RED, 0);
  analogWrite(LED_GREEN, 0);
  analogWrite(LED_BLUE, 0);
  currentRed = 0;
  currentGreen = 0;
  currentBlue = 0;
}

void getColor(){
  Serial.println("-----------------getColor()");
  String message = createResponse("\"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue));
  respond(message);
}

//-----------------------------helper--------------------------------
String createResponse(String data){
  if(data.length() == 0){
      return "{ \"success\": true}";
  }else{
    return "{ \"success\": true," + data + "}";
  }

}

String createError(String data){
  return "{ \"success\": false, " + data + "}";
}

void respond(String message){
  Serial.println(message);
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "text/plain", message);
}

void respondWithError(String error){
    Serial.println(error);
    server.send(500,"text/plain", error);
}

boolean checkBody(){
  /*if (server.hasArg("plain")== false){ //Check if body received
    Serial.println( "{ \"success\": false, \"message\": \"body is empty\"}");
    server.send(500, "text/plain", "{ \"success\": false, \"message\": \"body is empty\"}");
    return false;
  }*/
  return true;
}

boolean hasArg(String key){
 if (server.hasArg(key) == false){
    Serial.println( "{ \"success\": false, \"message\": \"" + key + " is empty\"}");
    server.send(500, "text/plain", "{ \"success\": false, \"message\": \"" + key + " is empty\"}");
    return false;
  }
  return true;
}

String getArg(String key){
  return server.arg(key);
}

void setup(void) {
  Serial.begin(9600);
  WiFi.begin(SSID, PASSWORD);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("try to connect to Wlan.");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(SSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/off", turnOff);

  server.on("/color", color);

  server.on("/", HTTP_OPTIONS, []() {
    server.sendHeader("Access-Control-Max-Age", "10000");
    server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
    server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    server.send(200, "text/plain", "" );
  });

  server.on("/getColor", getColor);

  //todo change duration

  server.begin();
  Serial.println("HTTP server started");

  pinMode(LED_RED, OUTPUT); 
  pinMode(LED_GREEN, OUTPUT); 
  pinMode(LED_BLUE, OUTPUT); 

}

void loop(void)
{
  //needed
  server.handleClient();
}
