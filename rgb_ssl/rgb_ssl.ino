
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServerSecure.h>
#include <ESP8266mDNS.h>

const char* SSID = "DosCerveza";
const char* PASSWORD = "5a4253d6";

//ESP8266WebServer server(80);
BearSSL::ESP8266WebServerSecure server(443);

static const char serverCert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDVzCCAj+gAwIBAgIJAKa7cKNxeyUTMA0GCSqGSIb3DQEBCwUAMEIxCzAJBgNV
BAYTAkRFMRMwEQYDVQQIDApTb21lLVN0YXRlMQ4wDAYDVQQKDAVtZWluczEOMAwG
A1UEAwwFTWFyY28wHhcNMTgxMTAxMTYyMDUxWhcNMjgxMDI5MTYyMDUxWjBCMQsw
CQYDVQQGEwJERTETMBEGA1UECAwKU29tZS1TdGF0ZTEOMAwGA1UECgwFbWVpbnMx
DjAMBgNVBAMMBU1hcmNvMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
2uyEYSBNaeezCNR3Dbwqavf6uUtPRXBLWeGxcGA9PXosqmGJR1LA0NUGduMzAfZo
mtuAhW/6Da7K0Jv9aIxmAzdxux/A6Z411u0nk8nriUJ/Z3DLVkt6IsNeTA45B624
AoyftQ81uy203Fyb3f6l1NAURLB44x0xrBextqSIfEhm2Bp6oO99/w3Ehf8aT7zS
T77yl+W3qWR+/e8hzAH6puEdbKs3tH3uAf31Fw1gDl4/q9z3Y2/dg1xOt5JzNnij
/bOO84PhivCql/uqVcpXYgGWbk/yGzPjJGlWCOR9e7cjA/UC3an4N4TQv1ZU02FY
nZm7oKW6bkruLbK1q2O22QIDAQABo1AwTjAdBgNVHQ4EFgQUSmdx1KH5uvm/EWgg
eTH7ZAIuC24wHwYDVR0jBBgwFoAUSmdx1KH5uvm/EWggeTH7ZAIuC24wDAYDVR0T
BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAMvuoFE8Y9IQxrj2a2hNOXqXvc5aW
gYYME559SsbGKTp7G90j/68/U5RnGCw6yzYlbRSQGfdkLEvWVyme1ghS0mrL44L8
P3qd+wxZjsOq8p40qEphEHy6x2j2t+2Fn+0CHDiKZXSevw1cmM6TzYotk10qsxVE
FcD+vis+fptD5u5xIk4XEmiBBBa3Jp2Oe15z+qKGFtLV+V2Q8GFYfvjnebF8+zUs
BfUudkQZV/6iorRYQLBzjQTqo4aMt58kBPWqiELeYzGTImGvwM6OlKFOTvtjnpU1
ZSCDPXKp9NzUCZvlUX6EWcmCqE4r0F6SNuFyB99/y1BfdyqgrdjzBiyfAA==
-----END CERTIFICATE-----
)EOF";

static const char serverKey[] PROGMEM =  R"EOF(
-----BEGIN ENCRYPTED PRIVATE KEY-----
MIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQIrDBZ5wql80cCAggA
MBQGCCqGSIb3DQMHBAinOSVwIyHhxwSCBMgGmhGHyyGrpOzSeL8L6gZgZLG8vdL6
cQ8c8+fu+swq+loJIXlNHikDFifaynleW6t7g9C+aRO27cEFrO1rN1nRed3wK1Py
QqKUq8MIFma3sLBfKoUNQSSAIQBJu9ARxaPxymT+UPKxLv+tGhK83vKVzP9lLlLt
Q0Nogd7KyxuWpDeM3gL5mr/wSaKjInQ74dxPC5i68pLTxHyipIp7ururdXIoy9D2
npUG4mM4qalqw9OZ0dyFbl9rE3MXBzc8OhI9KA0L7LeRpahJsM7qWFSs894Ej8Cj
q8f9WQMn5rpkl45A5yV2WPyeey5MnGDBlpmIGZxw/wbku4mNGItQc6cAsJwMUdRE
74o+xs7f6I2jjfDM+z9jjwHYnzujHmy0LQ4FCer88BCWmSMU1fnIN+E+UJ0BErvY
jT0lyogy5mvi30vkpcW8G/ouHGnw9rHUbtISUBFJXDoS80MgBBzoxlYaC490KfHQ
WiPF12IpENY2SeR/gD/XbusuDD7zcpHv3ZWVyRY+VqlyxGjFs681zWB54HD3CC1b
BDagUrM24iUoMAXDqBM3mgYMr5+hr4TGTXABj3g2Y+pTqM2RbuoLYYhHrx7eFntu
UCCR4l4qOmDQjYdYf+yTwof6pmj2iYmc5MY1nueYCUwMHZaV0hdS5smIdK+DY9H5
jP22ghSb+qrE0jb0gpOJh/RDfFgv6kIhtxKJEZE+/KcjQ4mrVYTCTn2qC1kvEl+B
u9LIBtds5zuSHOl2v/MuJe2vqa4Js/WzVoHMYGAXmOMeaPGozsCxtmFU0NJ8uYkt
p5s5bfN8DtIVhoT4bW4z+Ch7vRZQ2yVKJDRnXmiY7KP4XMmZSUUKjvyMatmY2OLA
Pb/c230G+TC7iM6ZYQ3KOrdSWvg1gQGk+kAC9o8Z3jKYvKZi+VrQ5svOKt6+m5I0
rwcIYNmOTNWO3Drn5SYy3iPF9A4cliJc+Gm19qAw5whEdQ7JQ7A2Ok/IoLEMuty2
HatOV4g3VKL715pTtP/771Y/iqnXYc8Cu6UTn1EkCoXOEiPUe45THn+yK6AIgln6
zoHFiZgVejjXS1G6nrl/r+TczPY8blGwmPbnOpG0OfUekyvx7BYjXG/y9wqt2pa4
/ykHx16a5nDQ8rb7A4zHZebMw6fClzXkpx+sMrbLrnRBQlM2ssLhU7UtPBvgEYZw
MHEJGtdxSleCT3rexSB1+VHtXF9kj9Zr1bltp3ii6trrNALLHmKZQdD45tE6HZsW
tOzbb5Pu4OCdnp2LjQZMaXlawyth8OT/POpKb6maWvF/It2iEasqFH3XDC/rx0rS
Q1SS1dAB3xT3CXqYY5AcMyxpnRdxXN/VmNxGYFFi4Ydehyb5pNAr8gB34GolCzfW
EJ7/h78gi8CvwzPeJ+VhlU5cOHz/mtKuYCuBJFTofFgAW0oW81zJLIy02XNsBdL0
cyiDqN761SuJGJg0F5dwMvtaLzAErHGuyKW1BjKfcxV57DQ99qzW+QIWFjPo31My
5JnJ4Ty/mxovrhvz0x6I76cG7SplZ1XubdwzOzf23P9OuIEmKtaJJDCcg4fVqY0r
ZLUiA5rNrnoavcXYTZtf7G6C8is1JEFT6vUdfusA81hgYN0OJwJgksreIyUu8swP
W5E=
-----END ENCRYPTED PRIVATE KEY-----
)EOF";

const int LED_RED = D3;
const int LED_GREEN = D8;
const int LED_BLUE = D5;
const int FADE = 5;

int currentRed = 0,
  currentGreen = 0, 
  currentBlue = 0;

//---------------------color----------------------------------
int fadeTo(int red, int green, int blue){
  int duration = 0;
  while(!hasColor(LED_RED, red) || !hasColor(LED_GREEN, green) || !hasColor(LED_BLUE, blue) ){
    fade(LED_RED, red);
    fade(LED_GREEN, green);
    fade(LED_BLUE, blue);
    delay(FADE);
    duration += FADE;
  }
  return duration;
}

int fade(int led, int color){
  int currentValue = getCurrentValue(led);
  if(!hasColor(led, color)){
      if(currentValue <= color){
        currentValue++;
      }else{
        currentValue--;
      }
      analogWrite(led, currentValue);
      setCurrentValue(led, currentValue);
  }
}

boolean hasColor(int led, int color){
  if(getCurrentValue(led) == color){
    return true;
  }else{
    return false;
  }
}


int getCurrentValue(int led){
   switch(led){
    case LED_RED:
      return currentRed;
    case LED_GREEN:
      return currentGreen;
    case LED_BLUE:
      return currentBlue;  
  }
}

int setCurrentValue(int led, int value){
  switch(led){
    case LED_RED:
      currentRed = value;
      break;
    case LED_GREEN:
      currentGreen = value;
      break;
    case LED_BLUE:
      currentBlue= value;
      break;  
  }
}

//-----------------------------http handler---------------------------
void color() {
    Serial.println("-----------------color()");
    if(checkBody()){
      if(hasArg("r"), hasArg("g"), hasArg("b")){
        // check parameter
        int red = getArg("r").toInt();
        int green = getArg("g").toInt();
        int blue = getArg("b").toInt();
        Serial.println("start: changing color: \"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue) + ",\"received\": { \"r\":" + String(red) + ", \"g\":" + String(green) + ", \"b\":" + String(blue) +"}");
        int duration = fadeTo(red, green, blue);
        Serial.println("finish: changed: \"duration\":" + String(duration) + ", \"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue) + ",\"received\": { \"r\":" + String(red) + ", \"g\":" + String(green) + ", \"b\":" + String(blue) +"}");
        String message = createResponse("\"duration\":" + String(duration) + ", \"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue) + ",\"received\": { \"r\":" + String(red) + ", \"g\":" + String(green) + ", \"b\":" + String(blue) +"}");
        respond(message);
      }
    }
}

void turnOff() {
  Serial.println("-----------------turnOff()");
  int duration = fadeTo(0,0,0);
  respond(createResponse("\"duration\": " + String(duration)));
  analogWrite(LED_RED, 0);
  analogWrite(LED_GREEN, 0);
  analogWrite(LED_BLUE, 0);
  currentRed = 0;
  currentGreen = 0;
  currentBlue = 0;
}

void getColor(){
  Serial.println("-----------------getColor()");
  String message = createResponse("\"r\":" + String(currentRed) + ", \"g\":" + String(currentGreen) + ", \"b\":" + String(currentBlue));
  respond(message);
}

//-----------------------------helper--------------------------------
String createResponse(String data){
  if(data.length() == 0){
      return "{ \"success\": true}";
  }else{
    return "{ \"success\": true," + data + "}";
  }

}

String createError(String data){
  return "{ \"success\": false, " + data + "}";
}

void respond(String message){
  Serial.println(message);
  //server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  //server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  //server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "text/plain", message);
}

void respondWithError(String error){
    Serial.println(error);
    server.send(500,"text/plain", error);
}

boolean checkBody(){
  /*if (server.hasArg("plain")== false){ //Check if body received
    Serial.println( "{ \"success\": false, \"message\": \"body is empty\"}");
    server.send(500, "text/plain", "{ \"success\": false, \"message\": \"body is empty\"}");
    return false;
  }*/
  return true;
}

boolean hasArg(String key){
 if (server.hasArg(key) == false){
    Serial.println( "{ \"success\": false, \"message\": \"" + key + " is empty\"}");
    server.send(500, "text/plain", "{ \"success\": false, \"message\": \"" + key + " is empty\"}");
    return false;
  }
  return true;
}

String getArg(String key){
  return server.arg(key);
}

void handleNotFound(){
  Serial.println("-----------------handleNotFound()");
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  Serial.begin(9600);
  WiFi.begin(SSID, PASSWORD);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("try to connect to Wlan.");
  }
  
  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");
 
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(SSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }
  
  server.setRSACert(new BearSSLX509List(serverCert), new BearSSLPrivateKey(serverKey));
  Serial.println("RSA Cert set");
  
  server.on("/off", turnOff);

  server.on("/color", color);

  server.on("/", HTTP_OPTIONS, []() {
    //server.sendHeader("Access-Control-Max-Age", "10000");
    //server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
    //server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    server.send(200, "text/plain", "" );
  });

  server.on("/getColor", getColor);

  server.onNotFound(handleNotFound);
  
  server.begin();
  Serial.println("HTTPs server started");

  pinMode(LED_RED, OUTPUT); 
  pinMode(LED_GREEN, OUTPUT); 
  pinMode(LED_BLUE, OUTPUT); 

}

void loop(void)
{
  //needed
  server.handleClient();
}
